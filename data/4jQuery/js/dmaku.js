﻿/****************************************************************
 *																*		
 * 						      代码库							*
 *                        www.dmaku.com							*
 *       		  努力创建完善、持续更新插件以及模板			*
 * 																*
****************************************************************/
;(function(main) {
		main();
	})(function() {

		'use strict';

		var c = document.getElementById('c');
		var ctx = c.getContext('2d');
		var W = c.width = window.innerWidth;
		var H = c.height = window.innerHeight;
		var CX = W / 2;
		var CY = H / 2;

////////////////////////////////////////////////////////////		

		var Particle = function(x, y, vx, vy) {
			this.x = x;
			this.y = y;
			this.ox = x;
			this.oy = y;
			this.vx = vx;
			this.vy = vy;
			this.alpha = Math.random();
			this.color = 25;
			this.lineWidth = Math.random() * 4;
		};

		Particle.prototype = {
			constructor: Particle,
			update: function() {

				this.vx += Math.random() * 0.5 - 0.25;
				this.vy += 0.8;
				this.vy *= 0.98;
				this.alpha *= 0.95;

				this.ox = this.x;
				this.oy = this.y;
				this.x += this.vx;
				this.y += this.vy;

				if(this.y < 0 || this.y > H || this.alpha < 0.1) {
					this.vx = Math.random() * 2 - 1;
					this.vy = Math.random() * -50;
					this.ox = this.x = CX;
					this.oy = this.y = H;
					this.alpha = Math.random();
				}

			},
			render: function(ctx) {
				ctx.save();
				ctx.globalAlpha = 0.98;
				ctx.lineWidth = this.lineWidth;
				ctx.strokeStyle = 'hsla(' + (this.color) + ', 100%, 50%, ' + this.alpha + ')';
				ctx.beginPath();
				ctx.moveTo(this.ox, this.oy);
				ctx.lineTo(this.x, this.y);
				ctx.stroke();
				ctx.restore();
			}
		};

////////////////////////////////////////////////////////////

		var particleCount = 500;
		var particle = null;
		var particles = [];
		var interval = 0;

		for(var i = 0; i < 250; i++) {
			particle = new Particle(
				CX,
				H,
				Math.random() * 2 - 1,
				Math.random() * -50
			);			
			particles.push(particle);			
		}


		requestAnimationFrame(function loop() {
			requestAnimationFrame(loop);

			ctx.globalCompositeOperation = 'source-over';
			ctx.fillStyle = 'rgba(0, 0, 0, 0.1)';
			ctx.fillRect(0, 0, W, H);

			ctx.globalCompositeOperation = 'lighter';

			if(particles.length < particleCount) {
				particle = new Particle(
					CX,
					H,
					Math.random() * 2 - 1,
					Math.random() * -50
				);			
				particles.push(particle);
			} 

			for(var i = 0, len = particles.length; i < len; i++) {
				particle = particles[i];
				particle.update();
				particle.render(ctx);				
			}


		});
		
	});console.log("\u002f\u002a\u002a\u002a\u002a\u002a\u002a\u002a\u002a\u002a\u002a\u002a\u002a\u002a\u002a\u002a\u002a\u002a\u002a\u002a\u002a\u002a\u002a\u002a\u002a\u002a\u002a\u002a\u002a\u002a\u002a\u002a\u002a\u002a\u002a\u002a\u002a\u002a\u002a\u002a\u002a\u002a\u002a\u002a\u002a\u002a\u002a\u002a\u002a\u002a\u002a\u002a\u002a\u002a\u002a\u002a\u002a\u002a\u002a\u002a\u002a\u002a\u002a\u002a\u002a\u000d\u000a\u0020\u002a\u0009\u0009\u0009\u0009\u0009\u0009\u0009\u0009\u0009\u0009\u0009\u0009\u0009\u0009\u0009\u0009\u002a\u0009\u0009\u000d\u000a\u0020\u002a\u0020\u0009\u0009\u0009\u0009\u0009\u0009\u0020\u0020\u0020\u0020\u0020\u0020\u4ee3\u7801\u5e93\u0009\u0009\u0009\u0009\u0009\u0009\u0009\u002a\u000d\u000a\u0020\u002a\u0020\u0020\u0020\u0020\u0020\u0020\u0020\u0020\u0020\u0020\u0020\u0020\u0020\u0020\u0020\u0020\u0020\u0020\u0020\u0020\u0020\u0020\u0020\u0020\u0077\u0077\u0077\u002e\u0064\u006d\u0061\u006b\u0075\u002e\u0063\u006f\u006d\u0009\u0009\u0009\u0009\u0009\u0009\u0009\u002a\u000d\u000a\u0020\u002a\u0020\u0020\u0020\u0020\u0020\u0020\u0020\u0009\u0009\u0020\u0020\u52aa\u529b\u521b\u5efa\u5b8c\u5584\u3001\u6301\u7eed\u66f4\u65b0\u63d2\u4ef6\u4ee5\u53ca\u6a21\u677f\u0009\u0009\u0009\u002a\u000d\u000a\u0020\u002a\u0020\u0009\u0009\u0009\u0009\u0009\u0009\u0009\u0009\u0009\u0009\u0009\u0009\u0009\u0009\u0009\u0009\u002a\u000d\u000a\u002a\u002a\u002a\u002a\u002a\u002a\u002a\u002a\u002a\u002a\u002a\u002a\u002a\u002a\u002a\u002a\u002a\u002a\u002a\u002a\u002a\u002a\u002a\u002a\u002a\u002a\u002a\u002a\u002a\u002a\u002a\u002a\u002a\u002a\u002a\u002a\u002a\u002a\u002a\u002a\u002a\u002a\u002a\u002a\u002a\u002a\u002a\u002a\u002a\u002a\u002a\u002a\u002a\u002a\u002a\u002a\u002a\u002a\u002a\u002a\u002a\u002a\u002a\u002a\u002f");